#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "converter.h"
#include "test_converter.h"
#include <QTest>

// Main loop. This is an entry point of application
int main(int argc, char *argv[])
{

    //register QML type "Converter" to use it in QML
    qmlRegisterType<Converter>("com.company.converter",1,0,"Converter");

    // Application Attributes
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    // The application class
    QGuiApplication app(argc, argv);

    // The QML Engine
    QQmlApplicationEngine engine;
    // The URL of the QML file
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    // Connecting signal and slot - making sure the object and URL match
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    // The engine loading QML file
    engine.load(url);

    QTest::qExec(new test_converter, argc, argv);
    // The application entering the event loop that keeps the applicaion open
    return app.exec();
}
