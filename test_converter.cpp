#include "test_converter.h"
#include <QtTest>
#include "converter.h"

test_converter::test_converter()
{

}

test_converter::~test_converter()
{

}
// unit test for function do_hexdump
void test_converter::do_hexdump()
{
    Converter conv;
       unsigned char msg[] = "Hello";
       // pass "Hello" as message for public key. Expect hex dump output
       QCOMPARE(conv.do_hexdump(msg, 5, 1) , QString(" 48 65 6c 6c 6f\n"));
       // pass "Hello" as message for private key. Expect hex dump output
       QCOMPARE(conv.do_hexdump(msg, 5, 2) , QString(" 48 65 6c 6c 6f\n"));
       // pass "Hello" as message for encrypted message. Expect hex dump output
       QCOMPARE(conv.do_hexdump(msg, 5, 3) , QString(" 48 65 6c 6c 6f\n"));
       // pass "Hello" as message for something undefined. Expect "hex dump "Error" at output
       QCOMPARE(conv.do_hexdump(msg, 5, 5) , QString("Error"));
}
// unit test for function executer
void test_converter::executer()
{
    Converter conv;
       // pass "Hello" for encryption-decryption. Expect "Hello" at output
       QCOMPARE(conv.executer(QString("Hello")) , QString("Hello"));
       // pass "Hello, it's a test" for encryption-decryption. Expect "Hello, it's a test" at output
       QCOMPARE(conv.executer(QString("Hello, it's a test")) , QString("Hello, it's a test"));
       // pass "/.4235 dfsdr23" for encryption-decryption. Expect "/.4235 dfsdr23" at output
       QCOMPARE(conv.executer(QString("/.4235 dfsdr23")) , QString("/.4235 dfsdr23"));
       // pass "123      fdsdf2" for encryption-decryption. Expect "123      fdsdf2" at output
       QCOMPARE(conv.executer(QString("123      fdsdf2")) , QString("123      fdsdf2"));
}
