#ifndef CONVERTER_H
#define CONVERTER_H

#include <QObject>
#include <QVariant>
#include "QList"
#include "Qt"

//Converter QObject class
class Converter : public QObject
{

    Q_OBJECT
    // Q Properties to be used in QML
    Q_PROPERTY(QString pubKey READ pubKey)
    Q_PROPERTY(QString secrKey READ secrKey)
    Q_PROPERTY(QString encrStr READ encrStr)
    Q_PROPERTY(QString decrStr READ decrStr)
// set of public function of Converter class
public:
    QString dumpedStr();
    QString pubKey();
    QString secrKey();
    QString encrStr();
    QString decrStr();
public slots:
    QString executer(QString);
    QString do_hexdump(unsigned char [], int, int);
// private functions of Converter class
private:
    int encrypt(unsigned char [], unsigned char [], int , unsigned char [], unsigned char [], unsigned char []);
    void decrypt(unsigned char [], unsigned char [], int , unsigned char [], unsigned char [], unsigned char []);
    QString m_encrStr;
    QString m_decrStr;
    QString m_secrKey;
    QString m_pubKey;
};

#endif // CONVERTER_H
