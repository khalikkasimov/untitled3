import QtQuick 2.9
import QtQuick.Window 2.2
import com.company.converter 1.0

Window {
    id: mainWindow
    visible: true
    color: "lightgrey"
    width: 1000
    height: 1000
    Text{
        id: enterStr
        font.pointSize: 22
        text: qsTr("Please enter 1-20 characters in the field below:")
    }
    // This is a C++ converter class
    Converter {
        id: converter
    }
    // User input rectangle which consists of 1-20 characters input string
    Rectangle{
        id: userInput
        width: 1000
        height: 100
        color: "lightgrey"
        border.width: 3
        border.color: "grey"
        anchors.top: enterStr.bottom
        TextInput{
            id: inputStr
            color:"black"
            cursorVisible: yes
            font.pointSize: 20
            anchors.fill: parent
            verticalAlignment: TextInput.AlignVCenter
            // max length is 20 characters
            maximumLength: 20
            // regExp do prohibit entering 0 characters
            validator: RegExpValidator { regExp: /.+/ }
            // press of ENTER button
            onAccepted: {
                //store input string into message
                var message = inputStr.getText(0,20)
                // call executer function of converter module and pass message into it
                converter.executer(message)
                //print encrypted public key in hex dump
                encrpk.text = converter.pubKey
                //print encrypted secret key in hex dump
                encrsk.text = converter.secrKey
                //print encrypted message in hex dump
                encrMsg.text = converter.encrStr
                // print decrypted message
                decrMsg.text = converter.decrStr
            }
        }
    }
    // GUI part for decrypted message
    Rectangle{
        x: 500
        y: 600
        width: 500
        height: 400
        border.width: 1
        border.color: "grey"
        color: "lightgrey"
        Text {
            id: decrMsg
            color:"black"
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 17
        }
    }
    // GUI part for encrypted message
    Rectangle{
        y: 600
        width: 500
        height: 400
        border.width: 1
        border.color: "grey"
        color: "lightgrey"
        Text {
            id: encrMsg
            color:"black"
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 12
        }
    }
    // GUI part for encrypted public key
    Rectangle{
        x: 500
        y: 135
        width: 500
        height: 465
        border.width: 1
        border.color: "grey"
        color: "lightgrey"
        Text {
            id: encrpk
            color:"black"
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 12
        }
    }
    // GUI part for encrypted secret key
    Rectangle{
        y: 135
        width: 500
        height: 465
        border.width: 1
        border.color: "grey"
        color: "lightgrey"
        Text {
            id: encrsk
            color:"black"
            anchors.fill: parent
            verticalAlignment: Text.AlignVCenter
            font.pointSize: 12
        }
    }
    // text string
    Text{
        id: pk
        font.pointSize: 12
        x: 50
        y: 150
        text: qsTr("Public Key: ")
    }
    // text string
    Text{
        id: sk
        font.pointSize: 12
        x: 550
        y: 150
        text: qsTr("Private Key: ")
    }
    // text string
    Text{
        id: em
        font.pointSize: 12
        x: 50
        y: 615
        text: qsTr("Encrypted Message: ")
    }
    // text string
    Text{
        id: dm
        font.pointSize: 12
        x: 550
        y: 615
        text: qsTr("Decrypted Message: ")
    }
}
