#ifndef TEST_CONVERTER_H
#define TEST_CONVERTER_H

#include <QObject>

class test_converter : public QObject
{
    Q_OBJECT

public:
    test_converter();
    ~test_converter();

private slots:
    void do_hexdump();
    void executer();
};
#endif // TEST_CONVERTER_H
