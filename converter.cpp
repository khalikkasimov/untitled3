#include "converter.h"
#include <stdio.h>
#include "string.h"
#include <iomanip>
#include "sstream"
#include <bits/stdc++.h>
#include "tweetnacl.h"
#include "randombytes.h"
#include <string.h>

using namespace std;

// Executer function definition
QString Converter::executer(QString message)
{
    //convert received message from QString format to std::string
    std::string convertedMsg = message.toUtf8().constData();
    char bufMsg[100];

    //definitions of arrays
    unsigned char publicKey[crypto_box_PUBLICKEYBYTES];
    unsigned char secretKey[crypto_box_SECRETKEYBYTES];
    unsigned char nonce[crypto_box_NONCEBYTES];
    unsigned char encrMsg[32];
    unsigned char decrMsg[32];

    //this is to ensure no data is stored in arrays
    memset(publicKey,'\0',crypto_box_PUBLICKEYBYTES);
    memset(secretKey,'\0',crypto_box_SECRETKEYBYTES);
    memset(nonce,'\0',crypto_box_NONCEBYTES);
    memset(encrMsg,'\0',32);
    memset(decrMsg,'\0',32);
    memset(bufMsg,'\0',32);
    //store content of received message in bufMsg
    strcpy(bufMsg, convertedMsg.c_str());

    // gets received message length
    unsigned long long encrlen = strlen(bufMsg);
    unsigned long long decrLength;

    // generate nonce
    randombytes(nonce, crypto_box_NONCEBYTES);
    // generate public-private key pair and store it in publicKey and secretKey
    crypto_box_keypair(publicKey,secretKey);
    // convert public and secret keys to hex dump
    do_hexdump(publicKey, crypto_box_PUBLICKEYBYTES, 1);
    do_hexdump(secretKey, crypto_box_SECRETKEYBYTES, 2);

    // encrypt received message with generated keys and nonce
    decrLength = encrypt(encrMsg, reinterpret_cast<unsigned char*>(bufMsg), encrlen, nonce, publicKey, secretKey);
    // convert encrypted message to hex dump
    do_hexdump(encrMsg, decrLength, 3);

    // decrypts encrypted message using generated public-private key pair and nonce
    decrypt(decrMsg, encrMsg, decrLength, nonce, publicKey, secretKey);
    //store decrypted message in QString format to m_decrStr
    m_decrStr = QString::fromUtf8((char*)decrMsg);

    //this is to ensure no data is stored in arrays
    memset(publicKey,'\0',crypto_box_PUBLICKEYBYTES);
    memset(secretKey,'\0',crypto_box_SECRETKEYBYTES);
    memset(nonce,'\0',crypto_box_NONCEBYTES);
    memset(encrMsg,'\0',32);
    memset(decrMsg,'\0',32);
    memset(bufMsg,'\0',32);
    return m_decrStr;
}

// do_hexdump function definition
QString Converter::do_hexdump(unsigned char pAddressIn[], int msgSize, int qmlStr)
{
    // fill buffer with zeros
    std::stringstream buffer;
    buffer << hex << setfill('0');

    int nread;
    for( nread = 1; nread < msgSize && pAddressIn[nread]; nread++ );

    // store converted hex values into buffer
    for( int i = 0; i < msgSize; i++ )
    {
        if(i % 16 == 0 && i !=0) buffer << "\n";
        else if( i % 8 == 0 && i !=0) buffer << ' ';
        if( i < nread )
            buffer << ' ' << setw(2) << (unsigned int)(unsigned char)pAddressIn[i];
        else
            buffer << "   ";
    }

    buffer << "\n";
    //switch to identify what has been converted to hex dump
    switch (qmlStr) {
    case 1:
        // update public key hex dump variable
        m_pubKey = QString::fromStdString(buffer.str());
        return m_pubKey;
        break;
    case 2:
        // update private key hex dump variable
        m_secrKey = QString::fromStdString(buffer.str());
        return m_secrKey;
        break;
    case 3 :
        // update encrypted message hex dump variable
        m_encrStr = QString::fromStdString(buffer.str());
        return m_encrStr;
        break;
    default:
        return QString("Error");
        break;
    }
}
// encrypt definition function
int Converter::encrypt(unsigned char encrmessage[], unsigned char message[], int length, unsigned char nonce[], unsigned char publicKey[], unsigned char secretKey[])
{
    // the following lines do masking due to tweetnacl library restrictions:
    // first crypto_box_ZEROBYTES of message should be 0
    unsigned char temp_msg[100];
    unsigned char temp_encr[100];
    memset(temp_msg,'\0',crypto_box_ZEROBYTES);
    memcpy(temp_msg + crypto_box_ZEROBYTES, message, length);
    // call tweetnacl crypto_box function which encrypts message temp_msg and stores encrypted result in temp_encr
    crypto_box(temp_encr, temp_msg, crypto_box_ZEROBYTES + length, nonce, publicKey, secretKey);
    // the following line does masking due to tweetnacl library restrictions:
    // first crypto_box_BOXZEROBYTES of ciphertext should be 0
    // Masked encrypted message is stored in encrmessage
    memcpy(encrmessage, temp_encr+crypto_box_BOXZEROBYTES, crypto_box_ZEROBYTES + length);
    // the return value is a length of encrypted message
    return crypto_box_ZEROBYTES + length - crypto_box_BOXZEROBYTES;
}
// decrypt definition function
void Converter::decrypt(unsigned char decrmessage[], unsigned char encrmessage[], int length, unsigned char nonce[], unsigned char publicKey[], unsigned char secretKey[])
{
    // the following lines do masking due to tweetnacl library restrictions:
    // first crypto_box_BOXZEROBYTES of ciphertext should be 0
    unsigned char temp_msg[100];
    unsigned char temp_encr[100];
    memset(temp_encr,'\0',crypto_box_BOXZEROBYTES);
    memcpy(temp_encr + crypto_box_BOXZEROBYTES, encrmessage, length);
    // call tweetnacl crypto_box_open function which decrypts message temp_encr and stores decrypted result in temp_msg
    crypto_box_open(temp_msg, temp_encr, crypto_box_BOXZEROBYTES + length, nonce, publicKey, secretKey);
    // the following lines do masking due to tweetnacl library restrictions:
    // first crypto_box_ZEROBYTES of decrypted message should be 0
    // Masked decrypted message is stored in decrmessage
    memcpy(decrmessage, temp_msg + crypto_box_ZEROBYTES, crypto_box_BOXZEROBYTES + length);
    decrmessage[crypto_box_BOXZEROBYTES + length -crypto_box_ZEROBYTES] = '\0';
}
// encrStr definition function
// returns m_encrStr
// function is called from QML
QString Converter::encrStr()
{
    return m_encrStr;
}
// decrStr definition function
// returns m_decrStr
// function is called from QML
QString Converter::decrStr()
{
    return m_decrStr;
}
// pubKey definition function
// returns m_pubKey
// function is called from QML
QString Converter::pubKey()
{
    return m_pubKey;
}
// secrKey definition function
// returns m_secrKey
// function is called from QML
QString Converter::secrKey()
{
    return m_secrKey;
}
